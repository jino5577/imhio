package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"bitbucket.org/jino5577/imhio/common"
	"fmt"
	"io/ioutil"
	"testing"
)

func setUpDB() (*gorm.DB, error) {
	dsn, err := ioutil.ReadFile("../dsn.txt")
	if err != nil {
		return nil, fmt.Errorf("error: getting dsn file: %s", err)

	}
	db, err := gorm.Open("postgres", string(dsn))
	if err != nil {
		return nil, fmt.Errorf("error: connecting to database: %s", err)
	}
	db.SingularTable(true)
	db.LogMode(false)
	return db, nil
}

func TestQuerySuccess(t *testing.T) {
	db, err := setUpDB()
	if err != nil {
		t.Fatal(err)
	}

	blockName := "Database.processing"
	confName := "Develop.mr_robot"
	q := NewQuery(db)

	params, err := q.GetParams(blockName, confName)
	if err != nil {
		t.Fatal(err)
	}
	if len(params) == 0 {
		t.Fatal("error: empty response")
	}

	m := make(map[string]interface{})
	err = q.ConvertToMap(params, m)
	if err != nil {
		t.Fatal(err)
	}

	db.Close()
}

func TestQueryBlockEmpty(t *testing.T) {
	db, err := setUpDB()
	if err != nil {
		t.Fatal(err)
	}

	blockName := ""
	confName := "Develop.mr_robot"
	q := NewQuery(db)

	_, err = q.GetParams(blockName, confName)
	if err != common.ErrBlockNotExists {
		t.Fatalf("error: expexted ErrBlockEmpty, got %s", err)
	}

	db.Close()
}

func TestQueryConfEmpty(t *testing.T) {
	db, err := setUpDB()
	if err != nil {
		t.Fatal(err)
	}

	blockName := ""
	confName := "Develop.mr_robot"
	q := NewQuery(db)

	_, err = q.GetParams(blockName, confName)
	if err != common.ErrBlockNotExists {
		t.Fatalf("error: expexted ErrBlockEmpty, got %s", err)
	}

	db.Close()
}
