package models

// Block table holds possible block parameter names
type Block struct {
	ID   uint64
	Name string
}

func (Block) TableName() string {
	return "block"
}
