package models

// Configuration table holds possible configuration names
type Configuration struct {
	ID   uint64
	Name string
}

func (Configuration) TableName() string {
	return "configuration"
}
