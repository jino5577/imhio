package models

// Attribute table holds possible configuration attributes
type Attribute struct {
	ID      uint64
	BlockID uint64
	Name    string
	Type    string
}

func (Attribute) TableName() string {
	return "attribute"
}
