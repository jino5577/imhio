package models

import (
	"bitbucket.org/jino5577/imhio/common"
	"fmt"
	"github.com/jinzhu/gorm"
	"strconv"
)

// query has methods to get confuguration parameters from database
type query struct {
	db *gorm.DB
}

// query constructor
func NewQuery(db *gorm.DB) *query {
	q := query{}
	q.db = db
	return &q
}

// GetParams get configuration parameters from database by block name and configuration name
func (q *query) GetParams(blockName, configurationName string) ([]common.Param, error) {

	// check block name existence
	block := Block{}
	if err := q.db.Where("name = ?", blockName).First(&block).Error; err != nil {
		return nil, common.ErrBlockNotExists
	}

	// check configuration name existence
	conf := Configuration{}
	if err := q.db.Where("name = ?", configurationName).First(&conf, &Configuration{Name: configurationName}).Error; err != nil {
		return nil, common.ErrConfNotExists
	}

	var results []common.Param

	// SQL query:
	//
	// SELECT a.name, a.type, v.value
	//   FROM attribute a
	//   INNER JOIN value v ON v.attribute_id = a.id
	//   WHERE a.block_id = ? and v.configuration_id = ?
	// 	 ;

	err := q.db.Table(fmt.Sprintf("%s a", Attribute{}.TableName())).
		Joins(fmt.Sprintf("INNER JOIN %s v ON v.attribute_id = a.id", Value{}.TableName())).
		Where("a.block_id = ?", block.ID).
		Where("v.configuration_id = ?", conf.ID).
		Select("a.name, a.type, v.value").
		Scan(&results).Error

	if err != nil {
		return nil, err
	}

	return results, nil
}

// ConvertToMap convert []common.Param to map for JSON marshalling
func (q *query) ConvertToMap(params []common.Param, m map[string]interface{}) error {
	for _, v := range params {
		switch v.Type {
		case "string":
			m[v.Name] = v.Value
		case "int":
			if i, err := strconv.Atoi(v.Value); err != nil {
				return fmt.Errorf("error: %s", err)
			} else {
				m[v.Name] = i
			}
		default:
			return common.ErrUnsupportedType
		}
	}
	return nil
}
