package models

// Value table holds configuration values
type Value struct {
	ID              uint64
	AttributeID     uint64
	ConfigurationID uint64
	Value           string
}

func (Value) TableName() string {
	return "value"
}
