package api

import "bitbucket.org/jino5577/imhio/common"

// Request represents incoming json structure
type Request struct {
	Conf  string `json:"Type"` // Configuration name
	Block string `json:"Data"` // Block parameters name
}

// ResponseError represents response error message
type ResponseError struct {
	Error  bool   `json:"Error,omitempty"`
	Errmsg string `json:"Errmsg,omitempty"`
}

// Validate validates request fields
func (r *Request) Validate() error {
	if len(r.Conf) == 0 && len(r.Block) == 0 {
		return common.ErrRequestEmpty
	}
	if len(r.Conf) == 0 {
		return common.ErrConfEmpty
	}
	if len(r.Block) == 0 {
		return common.ErrBlockEmpty
	}
	return nil
}
