package api

import (
	"bitbucket.org/jino5577/imhio/common"
	"testing"
)

func TestRequestValidation(t *testing.T) {

	req := Request{}

	if err := req.Validate(); err != common.ErrRequestEmpty {
		t.Fatal("empty request: expected ErrRequestEmpty")
	}

	req.Block = "Some data"
	req.Conf = ""
	if err := req.Validate(); err != common.ErrConfEmpty {
		t.Fatal("empty conf name in request: expected ErrConfEmpty")
	}

	req.Block = ""
	req.Conf = "Some data"
	if err := req.Validate(); err != common.ErrBlockEmpty {
		t.Fatal("empty block name in request: expected ErrBlockEmpty")
	}

}
