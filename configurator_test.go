package main

import (
	"bitbucket.org/jino5577/imhio/api"
	"bitbucket.org/jino5577/imhio/common"
	"bitbucket.org/jino5577/imhio/service"
	"bytes"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"github.com/smartystreets/goconvey/convey"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestConfigurator(t *testing.T) {

	convey.Convey("Top-level initialization", t, func() {

		// initialize database and http server
		dsn, err := ioutil.ReadFile("dsn.txt")
		if err != nil {
			log.Fatalf("error: getting dsn file: %s", err)
		}

		db, err := gorm.Open("postgres", string(dsn))
		if err != nil {
			log.Fatalf("error: connecting to database: %s", err)
		}
		defer db.Close()
		db.SingularTable(true)
		db.LogMode(false)

		svc := service.Get()
		svc.Init(db, string(dsn))

		httpServer := svc.Server()

		server := httptest.NewServer(httpServer)

		// start test cases
		convey.Convey("Given a request with correct data", func() {

			reqBody := `{"Type":"Develop.mr_robot","Data":"Database.processing"}`
			req := httptest.NewRequest("POST", "/", bytes.NewReader([]byte(reqBody)))
			resp := httptest.NewRecorder()

			convey.Convey("When the request is handled by the server", func() {
				httpServer.ServeHTTP(resp, req)

				convey.Convey("Then the response should be a 200", func() {
					convey.So(resp.Code, convey.ShouldEqual, http.StatusOK)
				})

				convey.Convey("Then the response contains no error", func() {
					respErr := api.ResponseError{}
					err := json.Unmarshal(resp.Body.Bytes(), &respErr)
					convey.So(err, convey.ShouldBeNil)
					convey.So(respErr.Error, convey.ShouldBeFalse)
					convey.So(respErr.Errmsg, convey.ShouldBeEmpty)
				})

				convey.Convey("Then the response contains correct answer", func() {
					mExpected := map[string]interface{}{
						"host":     "localhost",
						"port":     "5432",
						"database": "devdb",
						"user":     "mr_robot",
						"password": "secret",
						"schema":   "public",
					}
					m := make(map[string]interface{})
					err := json.Unmarshal(resp.Body.Bytes(), &m)
					convey.So(err, convey.ShouldBeNil)
					convey.So(m, convey.ShouldResemble, mExpected)
				})
			})
		})

		convey.Convey("Given a request with missing parameters", func() {

			reqBody := `{}`
			req := httptest.NewRequest("POST", "/", bytes.NewReader([]byte(reqBody)))
			resp := httptest.NewRecorder()

			convey.Convey("When the request is handled by the server", func() {
				httpServer.ServeHTTP(resp, req)

				convey.Convey("Then the response should be a 200", func() {
					convey.So(resp.Code, convey.ShouldEqual, http.StatusOK)
				})

				convey.Convey("Then the response contains specified error", func() {
					respErr := api.ResponseError{}
					err := json.Unmarshal(resp.Body.Bytes(), &respErr)
					convey.So(err, convey.ShouldBeNil)
					convey.So(respErr.Error, convey.ShouldBeTrue)
					convey.So(respErr.Errmsg, convey.ShouldEqual, common.ErrRequestEmpty.Error())
				})
			})
		})

		convey.Convey("Given a request with incorrect JSON", func() {

			reqBody := `123`
			req := httptest.NewRequest("POST", "/", bytes.NewReader([]byte(reqBody)))
			resp := httptest.NewRecorder()

			convey.Convey("When the request is handled by the server", func() {
				httpServer.ServeHTTP(resp, req)

				convey.Convey("Then the response should be a 400", func() {
					convey.So(resp.Code, convey.ShouldEqual, http.StatusBadRequest)
				})

				convey.Convey("Then the response contains error", func() {
					respErr := api.ResponseError{}
					err := json.Unmarshal(resp.Body.Bytes(), &respErr)
					convey.So(err, convey.ShouldBeNil)
					convey.So(respErr.Error, convey.ShouldBeTrue)
				})
			})
		})

		convey.Convey("Given a request with unexisting Type (conf name)", func() {

			reqBody := `{"Type":"Develop.mr_robot1","Data":"Database.processing"}`
			req := httptest.NewRequest("POST", "/", bytes.NewReader([]byte(reqBody)))
			resp := httptest.NewRecorder()

			convey.Convey("When the request is handled by the server", func() {
				httpServer.ServeHTTP(resp, req)

				convey.Convey("Then the response should be a 200", func() {
					convey.So(resp.Code, convey.ShouldEqual, http.StatusOK)
				})

				convey.Convey("Then the response contains specified error", func() {
					respErr := api.ResponseError{}
					err := json.Unmarshal(resp.Body.Bytes(), &respErr)
					convey.So(err, convey.ShouldBeNil)
					convey.So(respErr.Error, convey.ShouldBeTrue)
					convey.So(respErr.Errmsg, convey.ShouldEqual, common.ErrConfNotExists.Error())
				})
			})
		})

		convey.Convey("Given a request with unexisting Type (params block)", func() {

			reqBody := `{"Type":"Develop.mr_robot","Data":"Database.processing1"}`
			req := httptest.NewRequest("POST", "/", bytes.NewReader([]byte(reqBody)))
			resp := httptest.NewRecorder()

			convey.Convey("When the request is handled by the server", func() {
				httpServer.ServeHTTP(resp, req)

				convey.Convey("Then the response should be a 200", func() {
					convey.So(resp.Code, convey.ShouldEqual, http.StatusOK)
				})

				convey.Convey("Then the response contains specified error", func() {
					respErr := api.ResponseError{}
					err := json.Unmarshal(resp.Body.Bytes(), &respErr)
					convey.So(err, convey.ShouldBeNil)
					convey.So(respErr.Error, convey.ShouldBeTrue)
					convey.So(respErr.Errmsg, convey.ShouldEqual, common.ErrBlockNotExists.Error())
				})
			})
		})

		// This reset is run after each `Convey` at the same scope.
		convey.Reset(func() {
			server.Close()
		})

	})
}
