package service

import (
	"bitbucket.org/jino5577/imhio/api"
	"bitbucket.org/jino5577/imhio/common"
	"bitbucket.org/jino5577/imhio/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"log"
)

// service contains web server and his parameters, current database connection, and configurator interface
type service struct {
	port         string
	db           *gorm.DB
	server       *gin.Engine
	configurator common.Configurator
}

var svc service

// Get returns single copy of service
func Get() *service {
	return &svc
}

func (s *service) Init(db *gorm.DB, port string) {
	s.port = port
	s.db = db

	gin.SetMode(gin.ReleaseMode)
	s.server = gin.New()
	s.configurator = models.NewQuery(db)

	// handler function
	s.server.POST("/", func(c *gin.Context) {

		req := api.Request{}

		if err := c.BindJSON(&req); err != nil {
			log.Printf("error: bind json: %s", err)
			c.JSON(http.StatusOK, api.ResponseError{Error: true, Errmsg: common.CheckAPIError(err).Error()})
			return
		}

		if err := req.Validate(); err != nil {
			log.Printf("error: validate request: %s", err)
			c.JSON(http.StatusOK, api.ResponseError{Error: true, Errmsg: common.CheckAPIError(err).Error()})
			return
		}

		params, err := s.configurator.GetParams(req.Block, req.Conf)
		if err != nil {
			log.Printf("error: get confuguration parameters: %s", err)
			c.JSON(http.StatusOK, api.ResponseError{Error: true, Errmsg: common.CheckAPIError(err).Error()})
			return
		}

		m := make(map[string]interface{})
		err = s.configurator.ConvertToMap(params, m)
		if err != nil {
			log.Printf("error: convirting result to map: %s", err)
			c.JSON(http.StatusOK, api.ResponseError{Error: true, Errmsg: common.CheckAPIError(err).Error()})
			return
		}

		log.Printf("RESPONSE MAP: %#v", m)
		c.JSON(http.StatusOK, m)
	})

}

func (s *service) Server() *gin.Engine {
	return s.server
}
