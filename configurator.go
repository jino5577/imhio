package main

import (
	"bitbucket.org/jino5577/imhio/service"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"io/ioutil"
	"log"
)

const listenPort = "8081" // http server listen port

func main() {

	log.Print("Start")
	log.SetFlags(log.LstdFlags|log.Lshortfile)

	// file with connection string of PostgreSQL database for this executable file, tests, and migrator
	dsn, err := ioutil.ReadFile("dsn.txt")
	if err != nil {
		log.Fatalf("error: getting dsn file: %s", err)
	}

	db, err := gorm.Open("postgres", string(dsn))
	if err != nil {
		log.Fatalf("error: connecting to database: %s", err)
	}
	defer db.Close()
	db.SingularTable(true)
	db.LogMode(false)

	svc := service.Get()
	svc.Init(db, string(dsn))

	// http server listening
	if err := svc.Server().Run("0.0.0.0:" + listenPort); err != nil {
		log.Fatalf("error: HTTP server: %s", err)
	}

	log.Print("Exit")
}
