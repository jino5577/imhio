CREATE TABLE value
(
  id BIGSERIAL NOT NULL PRIMARY KEY,
  attribute_id BIGINT NOT NULL REFERENCES attribute(id) ON DELETE CASCADE,
  configuration_id BIGINT NOT NULL REFERENCES configuration(id) ON DELETE CASCADE,
  value VARCHAR NOT NULL
)
;

CREATE INDEX ON value (attribute_id)
;

CREATE INDEX ON value (configuration_id)
;

