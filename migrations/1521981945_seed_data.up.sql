INSERT INTO block(id, name) VALUES
  (1,'Database.processing'),
  (2, 'Rabbit.log')
;
ALTER SEQUENCE block_id_seq RESTART 3;

INSERT INTO configuration(id, name) VALUES
  (1, 'Develop.mr_robot'),
  (2, 'Test.vpn'),
  (3, 'Prod.vpn')
;
ALTER SEQUENCE configuration_id_seq RESTART 4;

INSERT INTO attribute (id, block_id, name, type) VALUES
  (1, 1, 'host', 'string'),
  (2, 1, 'port', 'string'),
  (3, 1, 'database', 'string'),
  (4, 1, 'user', 'string'),
  (5, 1, 'password', 'string'),
  (6, 1, 'schema', 'string'),
  (7, 2, 'host', 'string'),
  (8, 2, 'port', 'int'),
  (9, 2, 'virtualhost', 'string'),
  (10, 2, 'user', 'string'),
  (11, 2, 'password', 'string')
;
ALTER SEQUENCE attribute_id_seq RESTART 12;

INSERT INTO value (id, attribute_id, configuration_id, value) VALUES
  (1, 1, 1, 'localhost'),
  (2, 2, 1, '5432'),
  (3, 3, 1, 'devdb'),
  (4, 4, 1, 'mr_robot'),
  (5, 5, 1, 'secret'),
  (6, 6, 1, 'public'),
  (7, 7, 2, '10.0.5.42'),
  (8, 8, 2, '5671'),
  (9, 9, 2, '/'),
  (10, 10, 2, 'guest'),
  (11, 11, 2, 'guest'),
  (12, 7, 3, '10.0.5.42'),
  (13, 8, 3, '5671'),
  (14, 9, 3, '/'),
  (15, 10, 3, 'prod'),
  (16, 11, 3, 'prod')
;

ALTER SEQUENCE value_id_seq RESTART 17;