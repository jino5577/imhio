CREATE TABLE block
(
  id BIGSERIAL NOT NULL PRIMARY KEY,
  name VARCHAR NOT NULL
)
;

CREATE UNIQUE INDEX ON block (name)
;
