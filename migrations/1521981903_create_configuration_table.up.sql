CREATE TABLE configuration
(
  id BIGSERIAL NOT NULL PRIMARY KEY,
  name VARCHAR NOT NULL
)
;

CREATE UNIQUE INDEX ON configuration (name)
;
