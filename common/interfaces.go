package common

// Param represents single configuration parameter
type Param struct {
	Name  string
	Type  string
	Value string
}

// Configurator - interface for getting configuration values by Type and Data parameters
type Configurator interface {
	GetParams(blockName, configurationName string) ([]Param, error)
	ConvertToMap(params []Param, m map[string]interface{}) error
}
