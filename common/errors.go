package common

import "errors"

// list of api possible API errors
var (
	ErrRequestEmpty    = errors.New("error: request is empty")
	ErrConfEmpty       = errors.New("error: request: empty configuration")
	ErrBlockEmpty      = errors.New("error: request: empty parameters block")
	ErrConfNotExists   = errors.New("error: configuration does not exist")
	ErrBlockNotExists  = errors.New("error: parameters block does not exist")
	ErrUnsupportedType = errors.New("error: unsupported parameter type")
	ErrInternal        = errors.New("error: internal error")
)

// errorsList list of available api errors
var errorsList = []error{
	ErrRequestEmpty,
	ErrConfEmpty,
	ErrBlockEmpty,
	ErrConfNotExists,
	ErrBlockNotExists,
	ErrUnsupportedType,
	ErrInternal,
}

// CheckApiError returns only a limited number of API errors.
// If error not in list of available errors, returns ErrInternal
func CheckAPIError(err error) error {
	for _, v := range errorsList {
		if v == err {
			return err
		}
	}
	return err
}
