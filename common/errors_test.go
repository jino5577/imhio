package common

import (
	"errors"
	"testing"
)

func TestErrorsList(t *testing.T) {
	var err error

	err = ErrRequestEmpty
	if CheckAPIError(err) != err {
		t.Fatal("error: expected ErrRequestEmpty")
	}

	err = errors.New("test error")
	if CheckAPIError(err) != err {
		t.Fatal("error: expected ErrInternal")
	}
}
