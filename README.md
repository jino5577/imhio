# Configuration service (test task)
Service sends configuration parameters depending on request.


## Installation

#### Clone this repository

#### Database: PostgreSQL
Install or use docker/docker-compose containers, for example:
```
; run new container
docker run --name imhio-postgres -d -p 5432:5432 postgres:9
; run existing stopped container
docker start imhio-postgres
; stop container
docker stop imhio-postgres
```

Create new database:
```
CREATE DATABASE configurator;
```

Edit DSN in the `dsn.txt` file according to database configuration.

#### Database migration tool: [mattes/migrate](https://github.com/mattes/migrate)
CLI wrapper installation (in project directory):
```
go get -u -d github.com/mattes/migrate/cli github.com/lib/pq
go build -tags 'postgres' -o migrate github.com/mattes/migrate/cli
```

Apply migrations:
```
./migrate -path migrations/ -database "$(cat dsn.txt)" up
```

#### Testing framework: [GoConvey](https://github.com/smartystreets/goconvey)
Installation:
```
go get github.com/smartystreets/goconvey
```
Start up the GoConvey web server at the project's path:
```
$ $GOPATH/bin/goconvey
```
Then watch the test results display in the browser at:
```
http://localhost:8080
```

---
## Running
```
go build configurator.go
./configurator
```

## Description

Service listens 8081 port for POST HTTP requests.

Request format: JSON, for example:
```
{
"Type": "Develop.mr_robot",
"Data": "Database.processing"
}
```

Response format: JSON, http response code `200`.

Successful response example:
```
{
"host": "localhost",
"port": "5432",
"database": "devdb",
"user": "mr_robot",
"password": "secret",
"schema": "public"
}
```

Unsuccessful response contains two fields `Error` (`boolean`) and `Errmsg` (`string`):
```
{
"Error": true,
"Errmsg": "error: request is empty"
}
```

List of possible error messages:
```
error: request is empty
error: request: empty configuration
error: request: empty parameters block
error: configuration does not exist
error: parameters block does not exist
error: unsupported parameter type
error: internal error

```
